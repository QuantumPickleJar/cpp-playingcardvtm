
// Playing Cards Lab 
// Vincent Morrill (initial)

#include <iostream>
#include <conio.h>

using namespace std;


enum Suit {
	Hearts, Diamonds, Spades, Clubs
};

enum Rank {
	Two = 2, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King, Ace
	
	//Uncomment line below if Joker is not being used. Doing so will shift back to zero-based card ranks
	//Joker, One, Two, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King, Ace
};


//Structure for a Card
struct Card {
	Suit suit;
	Rank number;
};


int main()
{

	_getch();
	return 0;
}
